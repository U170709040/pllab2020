#include <stdio.h>


int gcd(int x, int y)
{

    if (x == 0)
       return y;
    if (y == 0)
       return x;


    if (x == y)
        return x;


    if (x > y)
        return gcd(x-y, y);
    return gcd(x, y-x);
}

int main()
{
    int x;
    int y;
    printf("Enter a positive integer number: ");
    scanf("%d",&x);
    printf("Enter a positive integer number: ");
    scanf("%d",&y);

    printf("GCD of %d and %d is %d ", x, y, gcd(x, y));
    return 0;
}
